package rip.devotedpvp.ffa.game;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import rip.devotedpvp.ffa.DevotedFFA;
import rip.devotedpvp.ffa.config.ConfigManager;
import rip.devotedpvp.ffa.gamemodes.Gamemode;
import rip.devotedpvp.ffa.gamemodes.UHCGamemode;
import rip.devotedpvp.ffa.message.MessageManager;
import rip.devotedpvp.ffa.message.PrefixType;
import rip.devotedpvp.ffa.utils.DatabaseUtils;
import rip.devotedpvp.ffa.utils.DiscordUtils;

public class Game {

    private FFAMode ffaMode;
    private Gamemode gameMode;
    private String worldName;
    private int maxPlayers;
    private boolean stats;

    public Game() {

        try {
            ffaMode = FFAMode.valueOf(ConfigManager.getConfig().getString("game.type"));
        } catch (Exception ex) {
            Bukkit.getLogger().warning("Setting the FFAMode to NONE as it can't find the saved one!");
            setFFAMode(FFAMode.NONE);
        }

        //initialize the gamemode
        switch (ffaMode) {
            case UHC:
                gameMode = new UHCGamemode();
                break;
            default:
                throw new RuntimeException("Unrecognized FFA TYPE in configuration file for FFA: " + ConfigManager.getConfig().getString("game.type"));
        }

        this.worldName = ConfigManager.getConfig().getString("settings.world", "arena");
        this.maxPlayers = ConfigManager.getConfig().getInt("settings.maxPlayers", 100);
        this.stats = ConfigManager.getConfig().getBoolean("settings.stats", true);

        if (isFFAMode(FFAMode.UHC)) {
            DatabaseUtils.setTable("uhcffa_stats");
        }

        DiscordUtils.sendStatusUpdate(ffaMode.name() + " FFA" + " initialized successfully!", DiscordUtils.SUCCESS_MSG);
        DiscordUtils.sendStatusUpdate("Players can now join " + ffaMode.name() + " FFA on " + DevotedFFA.get().getServerName(), DiscordUtils.SUCCESS_MSG);
    }

    public FFAMode getFFAMode() {
        return ffaMode;
    }

    public void setFFAMode(FFAMode ffaMode) {
        this.ffaMode = ffaMode;

        ConfigManager.getConfig().set("game.type", ffaMode.toString());
        ConfigManager.getInstance().saveConfig();
    }

    public boolean isFFAMode(FFAMode ffaMode) {
        return getFFAMode().equals(ffaMode);
    }

    public Gamemode getGameMode() {
        return this.gameMode;
    }

    public boolean isGamemode(Gamemode gameMode) {
        return getGameMode().equals(gameMode);
    }

    public String getWorldName() {
        return worldName;
    }

    public void setWorldName(String worldName) {
        this.worldName = worldName;

        ConfigManager.getConfig().set("settings.world", worldName);
        ConfigManager.getInstance().saveConfig();
    }

    public World getFFAWorld() {
        return Bukkit.getWorld(worldName);
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;

        ConfigManager.getConfig().set("settings.maxPlayers", maxPlayers);
        ConfigManager.getInstance().saveConfig();
    }

    public boolean isStatsEnabled() {
        return stats;
    }

    public void toggleStats() {
        if (isStatsEnabled()) {

            //disable stats
            setStats(false);

            MessageManager.broadcast(MessageManager.getPrefix(PrefixType.FFA) + "Stats are now disabled!");

        } else {

            //enable stats
           setStats(true);

            MessageManager.broadcast(MessageManager.getPrefix(PrefixType.FFA) + "Stats are now enabled!");
        }
    }

    public void setStats(boolean enable) {
        stats = enable;

        ConfigManager.getConfig().set("settings.stats", enable);
        ConfigManager.getInstance().saveConfig();
    }


    /**
     * Get the spawnpoint of the arena.
     *
     * @return The spawnpoint of the arena.
     */
    public Location getSpawn() {
        FileConfiguration config = ConfigManager.getConfig();

        World world = Bukkit.getWorld(config.getString("spawn.world", getWorldName()));

        if (world == null) {
            world = Bukkit.getWorlds().get(0);
        }

        double x = config.getDouble("spawn.x", 0);
        double y = config.getDouble("spawn.y", 51);
        double z = config.getDouble("spawn.z", 0);
        float yaw = (float) config.getDouble("spawn.yaw", 0);
        float pitch = (float) config.getDouble("spawn.pitch", 0);

        Location loc = new Location(world, x, y, z, yaw, pitch);
        return loc;
    }

}
