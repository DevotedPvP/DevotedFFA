package rip.devotedpvp.ffa;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import rip.devotedpvp.ffa.commands.CommandHandler;
import rip.devotedpvp.ffa.config.ConfigManager;
import rip.devotedpvp.ffa.config.DatabaseCredentials;
import rip.devotedpvp.ffa.data.StorageBackend;
import rip.devotedpvp.ffa.game.Game;
import rip.devotedpvp.ffa.gui.GUIManager;
import rip.devotedpvp.ffa.listeners.*;
import rip.devotedpvp.ffa.message.MessageManager;
import rip.devotedpvp.ffa.scoreboards.FFAScoreboard;
import rip.devotedpvp.ffa.tasks.ScoreboardTask;
import rip.devotedpvp.ffa.utils.DiscordUtils;

import java.util.logging.Level;

public class DevotedFFA extends JavaPlugin {

    private static boolean shutdown = false;

    private static DevotedFFA instance;
    private CommandHandler commandHandler;
    private Game game;
    private GUIManager gui;
    private String serverName;

    private static StorageBackend storageBackend;

    public void onEnable() {
        instance = this;

        ConfigManager.getInstance().setup();
        MessageManager.getInstance().setup();

        registerListeners(Bukkit.getPluginManager());

        //setup storage backend
        storageBackend = new StorageBackend(new DatabaseCredentials(
                ConfigManager.getConfig().getString("database.host"),
                ConfigManager.getConfig().getInt("database.port"),
                ConfigManager.getConfig().getString("database.user"),
                ConfigManager.getConfig().getString("database.pass"),
                ConfigManager.getConfig().getString("database.dbName")
                ));

        this.serverName = ConfigManager.getConfig().getString("name", "none");
        if (this.serverName.equals("none")) {
            getLogger().log(Level.SEVERE, "Server name not set, please set the server name before continuing.");
            Bukkit.getServer().shutdown();
        }

        DiscordUtils.sendStatusUpdate("Initializing FFA...", DiscordUtils.GENERAL_MSG);

        //load our game manager
        game = new Game();

        //setup the gui
        gui = new GUIManager(this);
        gui.registerGUIs();

        //register commands
        commandHandler = new CommandHandler(this);
        commandHandler.registerCommands(gui);

        ScoreboardTask.getInstance().runTaskTimer(this, 20, 20);

        try {
            game.getFFAWorld().setGameRuleValue("naturalRegeneration", "false");
            game.getFFAWorld().setGameRuleValue("doMobSpawning", "false");
            game.getFFAWorld().setGameRuleValue("doDaylightCycle", "false");
            game.getFFAWorld().setAutoSave(false);
        } catch (Exception ex) {
            getLogger().log(Level.SEVERE, "arena world does not exist or could not be found!");
        }

        /*if (getGame().isStatsEnabled()) {
            DatabaseUtils.loadTables();
        }*/
    }

    public void onDisable() {
        FFAScoreboard.getInstance().shutdown();
        getStorageBackend().closeConnections();
    }

    public static DevotedFFA get() {
        return instance;
    }

    private void registerListeners(PluginManager pm) {

        pm.registerEvents(ChatListener.getInstance(), this);
        pm.registerEvents(new SessionListener(), this);
        pm.registerEvents(new DeathListener(), this);
        pm.registerEvents(new CommandListener(), this);
        pm.registerEvents(new WorldListener(), this);

    }

    public Game getGame() {
        return game;
    }

    public void endSetup(String s) {
        getLogger().log(Level.SEVERE, s);
        if (!shutdown) {
            stop();
            shutdown = true;
        }
        throw new IllegalArgumentException("Disabling... " + s);
    }

    private void stop() {
        Bukkit.getServer().shutdown();
    }

    public GUIManager getGUI() {
        return gui;
    }

    public static StorageBackend getStorageBackend() {
        return storageBackend;
    }

    public static void registerListener(Listener l) {
        Bukkit.getPluginManager().registerEvents(l, instance);
    }

    public String getServerName() {
        return serverName;
    }
}
