package rip.devotedpvp.ffa.commands.admin;

import org.bukkit.command.CommandException;
import org.bukkit.command.CommandSender;
import rip.devotedpvp.ffa.DevotedFFA;
import rip.devotedpvp.ffa.commands.AbstractCommand;
import rip.devotedpvp.ffa.message.MessageManager;
import rip.devotedpvp.ffa.message.PrefixType;

import java.util.ArrayList;
import java.util.List;


public class TogglestatsCommand extends AbstractCommand {

	public TogglestatsCommand() {
		super("togglestats", "");
	}

	@Override
	public boolean execute(final CommandSender sender, final String[] args) throws CommandException {

		//toggle stats
		DevotedFFA.get().getGame().toggleStats();

		return true;
	}

	@Override
	public List<String> tabComplete(CommandSender sender, String[] args) {
		return new ArrayList<>();
	}
}