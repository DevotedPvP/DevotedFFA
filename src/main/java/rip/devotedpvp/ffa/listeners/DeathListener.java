package rip.devotedpvp.ffa.listeners;

import org.bukkit.ChatColor;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import rip.devotedpvp.ffa.DevotedFFA;
import rip.devotedpvp.ffa.game.FFAMode;
import rip.devotedpvp.ffa.kits.UHCKit;
import rip.devotedpvp.ffa.message.MessageManager;
import rip.devotedpvp.ffa.player.FFAPlayer;
import rip.devotedpvp.ffa.player.PlayerManager;
import rip.devotedpvp.ffa.utils.ItemStackUtils;

public class DeathListener implements Listener {

    @EventHandler (priority = EventPriority.HIGH)
    public void onDeath(PlayerDeathEvent event) {
        Player player = event.getEntity().getPlayer();
        Player killer = event.getEntity().getKiller();

        if (player == null || killer == null) {
            return;
        }

        //clear items of the player
        player.getInventory().clear();

        //update player stats
        FFAPlayer ffaPlayer = PlayerManager.getInstance().getFFAPlayer(player.getUniqueId());

        ffaPlayer.addDeath();
        ffaPlayer.addTotalDeath();

        //only reset kill streak if it is greater than 0
        if (ffaPlayer.getKillStreak() > 0) {
            MessageManager.broadcast(ChatColor.YELLOW + killer.getName() + ChatColor.DARK_AQUA + " has ended " + ChatColor.YELLOW +
                    player.getName() + ChatColor.DARK_AQUA + "'s killstreak of " + ChatColor.GOLD + ffaPlayer.getKillStreak() + ChatColor.DARK_AQUA + "!");

            if (ffaPlayer.getKillStreak() > ffaPlayer.getHighestKillStreak()) {
                ffaPlayer.setHighestKillStreak(ffaPlayer.getKillStreak());
            }

            ffaPlayer.resetKillStreak();
        }

        //killer stats
        killer.getInventory().addItem(ItemStackUtils.createGoldenHead());

        //update killer stats
        FFAPlayer ffaPlayerKiller = PlayerManager.getInstance().getFFAPlayer(killer.getUniqueId());

        ffaPlayerKiller.addKill();
        ffaPlayerKiller.addKillStreak();
        ffaPlayerKiller.addTotalKill();

        if (ffaPlayerKiller.getKillStreak() > ffaPlayerKiller.getHighestKillStreak()) {
            ffaPlayerKiller.setHighestKillStreak(ffaPlayerKiller.getKillStreak());
        }

        if (player.getLastDamageCause() != null) {

            EntityDamageEvent ede = player.getLastDamageCause();
            EntityDamageEvent.DamageCause dc = ede.getCause();

            if ((ede instanceof EntityDamageByEntityEvent)) {
                EntityDamageByEntityEvent edbee = (EntityDamageByEntityEvent) ede;
                Entity agresor = edbee.getDamager();

                if ((agresor instanceof Player)) {

                    event.setDeathMessage(ChatColor.RED + player.getName() + ChatColor.GRAY + " was slain by " + ChatColor.RED + killer.getName());
                    return;

                }

                if (dc == EntityDamageEvent.DamageCause.PROJECTILE) {
                    Projectile pro = (Projectile) agresor;
                    Entity shooter = (pro.getShooter() instanceof LivingEntity) ? (Entity)pro.getShooter() : null;

                    if (shooter == null) {
                        event.setDeathMessage(null);
                    } else {

                        if ((shooter instanceof Player)) {

                            if (pro.getType().equals(EntityType.ARROW)) {
                                event.setDeathMessage(ChatColor.RED + player.getName() + ChatColor.GRAY + " was shot by " + ChatColor.RED + killer.getName());
                            }

                        } else {
                            event.setDeathMessage(null);
                        }

                    }

                }

            } else {
                event.setDeathMessage(null);
            }

        }

        //finally we respawn them
        player.spigot().respawn();

    }

    @EventHandler (priority = EventPriority.HIGH)
    public void onRespawn(PlayerRespawnEvent event) {

        Player player = event.getPlayer();

        try {
            if (!DevotedFFA.get().getGame().isFFAMode(FFAMode.NONE)) {
                DevotedFFA.get().getGame().getGameMode().getKit().loadKit(player);
            }
        } catch (Exception ex) {
            //do nothing
        }

        event.setRespawnLocation(DevotedFFA.get().getGame().getSpawn());
    }

    @EventHandler
    public void onItemDrop(PlayerDropItemEvent event) {
        event.setCancelled(true);
    }

}
