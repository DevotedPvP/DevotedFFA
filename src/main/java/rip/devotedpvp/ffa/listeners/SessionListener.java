package rip.devotedpvp.ffa.listeners;


import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.ServerListPingEvent;
import rip.devotedpvp.ffa.DevotedFFA;
import rip.devotedpvp.ffa.game.FFAMode;
import rip.devotedpvp.ffa.message.MessageManager;
import rip.devotedpvp.ffa.message.PrefixType;
import rip.devotedpvp.ffa.player.PlayerManager;
import rip.devotedpvp.ffa.scoreboards.FFAScoreboard;

public class SessionListener implements Listener {

    @EventHandler (priority = EventPriority.HIGH)
    public void onJoin(PlayerJoinEvent event) {
        event.setJoinMessage(null);

        Player player = event.getPlayer();

        //make sure player is in survival and clear inventory
        player.setGameMode(GameMode.SURVIVAL);
        player.getInventory().clear();

        //make sure the player exists
        if (!PlayerManager.getInstance().doesFFAPlayerExsists(player.getUniqueId())) {
            PlayerManager.getInstance().createFFAPlayer(player.getUniqueId());
        }

        FFAScoreboard.getInstance().addScoreboard(player);
        FFAScoreboard.getInstance().newScoreboard(player);

        player.teleport(DevotedFFA.get().getGame().getSpawn());

        if (!DevotedFFA.get().getGame().isFFAMode(FFAMode.NONE)) {
            DevotedFFA.get().getGame().getGameMode().getKit().loadKit(player);
        }

    }

    @EventHandler (priority = EventPriority.HIGH)
    public void onQuit(PlayerQuitEvent event) {
        event.setQuitMessage(null);

        Player player = event.getPlayer();

        player.getInventory().clear();
        PlayerManager.getInstance().removeFFAPlayer(player.getUniqueId());
        FFAScoreboard.getInstance().removeScoreboard(player);
    }

    @EventHandler (priority = EventPriority.HIGH)
    public void onKick(PlayerKickEvent event) {

        Player player = event.getPlayer();

        player.getInventory().clear();
        PlayerManager.getInstance().removeFFAPlayer(player.getUniqueId());
        FFAScoreboard.getInstance().removeScoreboard(player);
    }

    @EventHandler (priority = EventPriority.HIGH)
    public void onLogin(PlayerLoginEvent event) {
        Player player = event.getPlayer();

        if (Bukkit.getServer().getOnlinePlayers().size() >= DevotedFFA.get().getGame().getMaxPlayers()) {
            player.kickPlayer(MessageManager.getPrefix(PrefixType.FFA) + "The server is currently full, if you wish to join full servers,\n" +
                    "you can buy a donor rank at http://store.devotedpvp.rip");
        }
    }

    @EventHandler
    public void onPing(ServerListPingEvent event) {

        event.setMotd(MessageManager.getPrefix(PrefixType.FFA) + "§7UHC FFA now in public beta");
        event.setMaxPlayers(DevotedFFA.get().getGame().getMaxPlayers());
    }

}
