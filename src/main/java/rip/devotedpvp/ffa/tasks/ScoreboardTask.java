package rip.devotedpvp.ffa.tasks;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import rip.devotedpvp.ffa.DevotedFFA;
import rip.devotedpvp.ffa.game.FFAMode;
import rip.devotedpvp.ffa.scoreboards.FFAScoreboard;

public class ScoreboardTask extends BukkitRunnable {

    private static ScoreboardTask instance;

    public static ScoreboardTask getInstance() {
        if (instance == null) {
            instance = new ScoreboardTask();
        }
        return instance;
    }

    @Override
    public void run() {

        for (Player online : Bukkit.getServer().getOnlinePlayers()) {
            FFAScoreboard.getInstance().newScoreboard(online);
        }

    }

}
