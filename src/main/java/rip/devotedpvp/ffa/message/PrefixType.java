package rip.devotedpvp.ffa.message;

public enum PrefixType {

    MAIN,
    ARROW,
    PERMISSIONS,
    FFA,

}
