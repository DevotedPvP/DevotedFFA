package rip.devotedpvp.ffa.kits;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Soup extends FFAKit {

    public Soup() {
        super("Soup");

        // Potion effects
        this.potionEffects.add(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 1)); // Speed 2
        this.potionEffects.add(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 1)); // Strength 2

        // Create default armor kit
        this.armorItems[3] = new ItemStack(Material.DIAMOND_HELMET);
        this.armorItems[3].addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        this.armorItems[2] = new ItemStack(Material.DIAMOND_CHESTPLATE);
        this.armorItems[2].addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        this.armorItems[1] = new ItemStack(Material.DIAMOND_LEGGINGS);
        this.armorItems[1].addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        this.armorItems[0] = new ItemStack(Material.DIAMOND_BOOTS);
        this.armorItems[0].addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        this.armorItems[0].addEnchantment(Enchantment.PROTECTION_FALL, 4);

        // Create default inventory kit
        this.inventoryItems[0] = new ItemStack(Material.DIAMOND);
        this.inventoryItems[0].addEnchantment(Enchantment.DAMAGE_ALL, 1);
        for (int i = 2; i < 9; i++ ) {
            this.inventoryItems[i] = new ItemStack(Material.MUSHROOM_SOUP);
        }
        for (int i = 27; i < 36; i++ ) {
            this.inventoryItems[i] = new ItemStack(Material.MUSHROOM_SOUP);
        }
    }

}
