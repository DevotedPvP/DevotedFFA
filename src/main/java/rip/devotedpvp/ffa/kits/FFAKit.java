package rip.devotedpvp.ffa.kits;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import rip.devotedpvp.ffa.DevotedFFA;
import rip.devotedpvp.ffa.events.KitLoadEvent;

import java.util.ArrayList;
import java.util.List;

public class FFAKit {

    private String name;
    protected ItemStack[] inventoryItems = new ItemStack[36];
    protected ItemStack[] armorItems = new ItemStack[9];
    protected List<PotionEffect> potionEffects = new ArrayList<>();

    public FFAKit(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public ItemStack[] getInventoryItems(){
        return this.inventoryItems;
    }
    public ItemStack[] getArmorItems(){
        return this.armorItems;
    }

    public void loadKit(Player player) {

        try {

            if (!potionEffects.isEmpty()) {
                for (PotionEffect effect : this.potionEffects) {
                    player.addPotionEffect(effect);
                }
            }

            player.getInventory().setContents(this.inventoryItems);
            player.getInventory().setArmorContents(this.armorItems);
            player.updateInventory();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        //call the KitLoadEvent
        Bukkit.getScheduler().runTaskAsynchronously(DevotedFFA.get(), new Runnable() {
            @Override
            public void run() {
                Bukkit.getServer().getPluginManager().callEvent(new KitLoadEvent(player, DevotedFFA.get().getGame().getGameMode().getKit()));
            }
        });
    }

}
