package rip.devotedpvp.ffa.kits;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import rip.devotedpvp.ffa.utils.ItemStackUtils;

public class SG extends FFAKit {

    public SG() {
        super("SG");

        //create default armor kit
        this.armorItems[3] = new ItemStack(Material.GOLD_HELMET);
        this.armorItems[2] = new ItemStack(Material.IRON_CHESTPLATE);
        this.armorItems[1] = new ItemStack(Material.CHAINMAIL_LEGGINGS);
        this.armorItems[0] = new ItemStack(Material.IRON_BOOTS);

        // Create default inventory kit
        this.inventoryItems[0] = new ItemStack(Material.STONE_SWORD);
        this.inventoryItems[1] = new ItemStack(Material.FISHING_ROD);
        this.inventoryItems[2] = new ItemStack(Material.BOW);
        this.inventoryItems[3] = new ItemStack(Material.GOLDEN_APPLE);
        this.inventoryItems[4] = new ItemStack(Material.GOLDEN_CARROT);
        this.inventoryItems[5] = new ItemStack(Material.PUMPKIN_PIE, 2);
        this.inventoryItems[6] = new ItemStack(Material.MELON, 2);
        this.inventoryItems[7] = new ItemStack(Material.BREAD);
        this.inventoryItems[8] = new ItemStack(Material.FLINT_AND_STEEL);
        this.inventoryItems[9] = new ItemStack(Material.ARROW, 8);

    }

}
