package rip.devotedpvp.ffa.kits;

import org.bukkit.entity.Player;
import rip.devotedpvp.ffa.DevotedFFA;

import java.util.ArrayList;
import java.util.List;

public class KitManager {

    private static KitManager instance;

    public static KitManager get() {
        if (instance == null) {
            instance = new KitManager();
        }
        return instance;
    }

    private List<FFAKit> kits = new ArrayList<>();

    public void loadKits() {
        kits.add(new UHC());
    }

    public FFAKit getKit(String name) {
        for (FFAKit kit : kits) {
            if (kit.getName().equals(name)) {
                return kit;
            }
        }
        return null;
    }

    public void giveKit(Player player) {

        switch (DevotedFFA.get().getGame().getFFAMode()) {
            case UHC:
                getKit("UHC").loadKit(player);
        }

    }

}
