package rip.devotedpvp.ffa.data.runnable;

public interface GenericCallback {
    void call(boolean result);
}
