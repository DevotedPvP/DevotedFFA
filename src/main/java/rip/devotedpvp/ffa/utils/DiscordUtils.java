package rip.devotedpvp.ffa.utils;

import com.mrpowergamerbr.temmiewebhook.DiscordEmbed;
import com.mrpowergamerbr.temmiewebhook.DiscordMessage;
import com.mrpowergamerbr.temmiewebhook.TemmieWebhook;
import rip.devotedpvp.ffa.DevotedFFA;

import java.util.Arrays;

public class DiscordUtils {

    private static String statusHookURl = "https://canary.discordapp.com/api/webhooks/360531223413784579/Y_oEl_IyBHMXy0mxsgtiA6F3LkkyfiyWl3x3-CMJ8UOcKxW36-hhl4OOF0c0PozOLVvU";

    public static final int GENERAL_MSG = 3447003;
    public static final int SUCCESS_MSG = 0x23c540;
    public static final int ERROR_MSG = 0xfd0006;
    public static final int WARNING_MSG = 0xfff30d;

    public static void sendStatusUpdate(String message) {
        TemmieWebhook temmie = new TemmieWebhook(statusHookURl);

        DiscordEmbed de = DiscordEmbed.builder()
                .title("FFA Status") // We are creating a embed with this title...
                .description(message)
                .build();

        DiscordMessage dm = DiscordMessage.builder()
                .username(DevotedFFA.get().getServerName()) // We are creating a message with the username "DevotedStatus Bot"...
                .content("") // with no content because we are going to use the embed...
                .embeds(Arrays.asList(de)) // with the our embed...
                .build(); // and now we build the message!

        temmie.sendMessage(dm);
    }

    public static void sendStatusUpdate(String message, int color) {
        TemmieWebhook temmie = new TemmieWebhook(statusHookURl);

        DiscordEmbed de = DiscordEmbed.builder()
                .title("FFA Status") // We are creating a embed with this title...
                .description(message)
                .color(color)
                .build();

        DiscordMessage dm = DiscordMessage.builder()
                .username(DevotedFFA.get().getServerName()) // We are creating a message with the username "DevotedStatus Bot"...
                .content("") // with no content because we are going to use the embed...
                .embeds(Arrays.asList(de)) // with the our embed...
                .build(); // and now we build the message!

        temmie.sendMessage(dm);
    }


}
